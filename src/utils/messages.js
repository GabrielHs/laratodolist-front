export default {
    ServerError: 'Ops! Alguma coisa deu errado.',
    LoginInvalidException: 'Ops! E-mail e/ou senha inválidos.',
    UserHasBeenTakenException: 'Ops! Parece que este usuário já existe.',
    VerifyEmailTokenException: 'Ops! Parece que este token está inválido.',
    NotFoundException: 'Ops! Não encontrado dados baseado nesse registro.',
    ResetPasswordTokenInvalidException: 'Ops! Parece que este token está inválido.',
};
