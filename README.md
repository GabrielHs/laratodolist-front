# todo list com vuejs app
![Alt text](./img1.PNG)
![Alt text](./img2.PNG)
![Alt text](./img3.PNG)



## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
